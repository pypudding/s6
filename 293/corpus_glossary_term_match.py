import pandas as pd
import re 
from pathlib import Path
import os

def corpus_glossary_match(corpus_file, 
                            corpus_column, 
                            glossary_term_file, 
                            glossary_term_column, 
                            output_path,
                            keep_other_columns=False):
    """
    display glossary terms next to corpus content

    Args:
        corpus_file (str): path to corpus file
        corpus_column (str): corpus column name
        glossary_term_file (str): path to glossary file
        glossary_term_column (str): glossary term column name
        output_path (str): output file parent folder
        keep_other_columns (bool, optional): if True, other columns in both input files 
                                            will be saved in the output. Else only corpus column 
                                            and glossary column will be saved in the output.
                                            Defaults to False.
    """

    df_corpus = pd.read_csv(corpus_file, index_col=False)
    df_glossary = pd.read_csv(glossary_term_file, index_col=False)

    df_output = pd.DataFrame()
    if keep_other_columns:
        columns_to_remove = ['Unnamed: 0', glossary_term_column, corpus_column]
        # create an empty dataframe to hold data from other columns
        df_other_columns_glossary = pd.DataFrame({c: [] 
                                        for c in df_glossary.columns
                                        if c not in columns_to_remove})
        df_other_columns_corpus = pd.DataFrame({c: [] 
                                        for c in df_corpus.columns
                                        if c not in columns_to_remove})
    corpus= []
    corpus_id = []
    terms = []
    count = 0
    total = len(df_glossary[glossary_term_column])
    for i, term in enumerate(df_glossary[glossary_term_column]):

        # get corpus sentences that contain term
        contained_terms = df_corpus[corpus_column]\
                            .str.contains(r'\b'+re.escape(term)+r'\b', case=False)

        contained_terms = contained_terms.where(contained_terms > 0).dropna()

        # save sentences id from original corpus file for sorting purpose later
        corpus_id.extend(contained_terms.index.tolist())
        # save sentences to list
        corpus.extend(df_corpus[corpus_column][contained_terms.index].tolist())
        # save terms to list
        terms.extend([term] * len(contained_terms))

        # save data from other columns into df
        if keep_other_columns:
            df_other_columns_glossary = df_other_columns_glossary.append(
                pd.DataFrame({c:[df_glossary.loc[i, c]]*len(contained_terms)
                    for c in df_other_columns_glossary.columns}),ignore_index=True)
            
            df_other_columns_corpus = df_other_columns_corpus.append(
                 pd.DataFrame({c:df_corpus.loc[j, c]
                    for c in df_other_columns_corpus.columns}
                    for j in contained_terms.index),ignore_index=True)
        count += 1
        print(f'finished term: {count}/{total}')
    
    # save to df
    df_output['corpus_id'] = corpus_id
    df_output[corpus_column] = corpus
    df_output[glossary_term_column] = terms


    if keep_other_columns:
        df_output = pd.concat([df_output, df_other_columns_corpus, df_other_columns_glossary], axis=1)

    
    df_output = df_output.sort_values(by='corpus_id').reset_index(drop=True)

    output_name = Path(corpus_file).resolve().stem + '_' + Path(glossary_term_file).resolve().stem
    df_output.to_csv(os.path.join(output_path, output_name +'.csv'))


if __name__ == '__main__':
    corpus = 'inputs/best_surfboards_ever_sentences.csv'
    glossary = 'inputs/best_surfboards_ever_glossary_terms.csv'
    corpus_glossary_match(corpus, 'sentences', glossary, 'glossary_terms', './output', True)